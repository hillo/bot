exports.parseMessages = (team,messages) ->
  result = []
  team = team._client.users
  messages.forEach (item, index) ->
    console.log '---------------', item
    result.push exports.parseMessage(team,item)
  result

exports.parseMessage = (team,message) ->
  messageObject = {}
  messageData = message.text.split(';')
  console.log messageData
  messageObject =
    user: team[message.user].name
    date: messageData[0]
    hours: parseFloat messageData[1], 2
    text: messageData[3]
